import thunk from 'redux-thunk';
import {
	compose, createStore, combineReducers, applyMiddleware
} from 'redux';
import { connectRouter, routerMiddleware } from 'connected-react-router';
import history from 'routerHistory';
import * as reducers from './reducers';

/* eslint-disable no-underscore-dangle */
// const reduxDevToolsExtension = window.__REDUX_DEVTOOLS_EXTENSION__;
/* eslint-enable */

const combinedReducers = combineReducers({
	...reducers,
	router: connectRouter(history)
});

/* eslint-disable no-underscore-dangle */
const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
/* eslint-enable no-underscore-dangle */

const store = createStore(
	combinedReducers,
	composeEnhancer(
		applyMiddleware(
			routerMiddleware(history),
			thunk
		)
	)
);

export default store;
