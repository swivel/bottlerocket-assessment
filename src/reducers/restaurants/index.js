import {
	RESTAURANTS_LOADING,
	RESTAURANTS_RECEIVED,
	RESTAURANTS_ERROR
} from 'actions/types';

const initialState = {
	restaurants: [],
	error: false,
	loading: false
};

const restaurantsReducer = (state = initialState, action) => {
	switch (action.type) {
		case RESTAURANTS_LOADING:
			return {
				...state,
				loading: true,
				error: false,
				restaurants: []
			};
		case RESTAURANTS_RECEIVED:
			return {
				...state,
				loading: false,
				error: false,
				restaurants: action.payload
			};
		case RESTAURANTS_ERROR:
			return {
				...state,
				loading: false,
				error: true,
				restaurants: []
			};
		default:
			return state;
	}
};

export default restaurantsReducer;
