import { MAPS_API_KEY } from 'actions/types';

const initialState = {
	mapsApiKey: ''
};

const mapsApiReducer = (state = initialState, action) => {
	switch (action.type) {
		case MAPS_API_KEY:
			return { mapsApiKey: action.payload };
		default:
			return state;
	}
};

export default mapsApiReducer;
