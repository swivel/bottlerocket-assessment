import React from 'react';

// Styled
import HeaderStyled from 'styled/global/header';
import HeaderTitle from 'styled/global/header/title';
import IconLink from 'styled/global/header/iconLink';

const Header = ({ title, backTo, secondary = {} }) => (
	<HeaderStyled>
		<IconLink to={backTo} icon="ic_webBack" />
		<HeaderTitle>
			{title}
		</HeaderTitle>
		<IconLink to={secondary.to} icon={secondary.icon} />
	</HeaderStyled>
);

export default Header;
