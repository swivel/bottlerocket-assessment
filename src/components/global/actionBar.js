import React from 'react';

// Components
import ActionBarStyled from 'styled/global/actionBar';
import Link from 'styled/global/actionBar/link';

const ActionBar = () => (
	<ActionBarStyled>
		<Link to="/lunch" icon="tab_lunch">Lunch</Link>
		<Link to="/internets" icon="tab_internets">Internets</Link>
	</ActionBarStyled>
);

export default ActionBar;
