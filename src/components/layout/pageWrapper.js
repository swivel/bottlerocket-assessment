import React from 'react';

// Components
import ActionBar from 'components/global/actionBar';

// Styled
import PageWrapperStyled from 'styled/layout/pageWrapper';
import PageLayout from 'styled/layout/pageLayout';

class PageWrapper extends React.Component {
	componentDidMount() {
		const { onPageLoad = (() => {}) } = this.props;
		onPageLoad();
	}

	render() {
		const { children } = this.props;
		return (
			<PageWrapperStyled>
				<PageLayout>
					{children}
				</PageLayout>

				<ActionBar />
			</PageWrapperStyled>
		);
	}
}

export default PageWrapper;
