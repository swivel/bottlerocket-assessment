import React from 'react';

// Styled
import RestaurantsWrapperStyled from 'styled/restaurants/wrapper';

// Components
import Restaurant from './partials/restaurant';

const RestaurantsWrapper = (props) => {
	const { restaurants } = props;

	const restaurantsList = restaurants.map(r => (
		<Restaurant
			key={r.name}
			{...r}
		/>
	));

	return (
		<RestaurantsWrapperStyled>
			{restaurantsList}
		</RestaurantsWrapperStyled>
	);
};

export default RestaurantsWrapper;
