import React from 'react';

import RestaurantStyled from 'styled/restaurants/restaurant';

// Component
const Restaurant = ({ name, category, backgroundImageURL }) => (
	<RestaurantStyled to={`/lunch/${name.replace(/\s+/g, '_')}`}>
		<img src={backgroundImageURL} alt={name} />
		<h1>{name}</h1>
		<strong>{category}</strong>
	</RestaurantStyled>
);

export default Restaurant;
