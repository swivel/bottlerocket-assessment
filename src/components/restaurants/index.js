import { connect } from 'react-redux';

// Component
import RestaurantsWrapper from './restaurantsWrapper';

const mapStateToProps = ({ restaurantsReducer }) => ({
	restaurants: restaurantsReducer.restaurants
});

export default connect(mapStateToProps)(RestaurantsWrapper);
