import React from 'react';
import GoogleMap from 'google-map-react';

import MapMarker from 'styled/svgs/mapMarker';

// Styled
import MapsContainer from 'styled/maps';

const Map = ({ mapsApiKey, lat, lng }) => (
	<MapsContainer>
		{mapsApiKey && lat && lng ? (
			<GoogleMap
				bootstrapURLKeys={{ key: mapsApiKey }}
				defaultCenter={[lat, lng]}
				center={[lat, lng]}
				zoom={17}
			>
				<MapMarker lat={lat} lng={lng} />
			</GoogleMap>
		) : 'Loading...'}
	</MapsContainer>
);

export default Map;
