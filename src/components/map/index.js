import { connect } from 'react-redux';

import Map from './map';

const mapStateToProps = ({ mapsApiReducer }) => ({ ...mapsApiReducer });

export default connect(mapStateToProps)(Map);
