import { bottlerocket } from 'services/rest';

import {
	RESTAURANTS_LOADING,
	RESTAURANTS_RECEIVED,
	RESTAURANTS_ERROR
} from './types';

export const getRestaurants = () => (dispatch) => {
	dispatch({ type: RESTAURANTS_LOADING });
	return bottlerocket.getRestaurants()
		.then(restaurants => dispatch({
			type: RESTAURANTS_RECEIVED,
			payload: restaurants
		}))
		.catch((e) => {
			/* eslint-disable no-console */
			console.error(e);
			/* eslint-enable */
			dispatch({ type: RESTAURANTS_ERROR });
		});
};
