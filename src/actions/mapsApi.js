import { MAPS_API_KEY } from 'actions/types';

import { getAppConfig } from 'services/config';

export const getMapsApiKey = () => dispatch =>
	getAppConfig()
		.then(({ mapsApi }) => dispatch({
			type: MAPS_API_KEY,
			payload: mapsApi
		}));
