import React from 'react';
import PageWrapper from 'components/layout/pageWrapper';

const NotFound = () => (
	<PageWrapper>
		<h1>404 - Not Found</h1>
		<p>
			The page you&apos;re looking for doesn&apos;t exist.
		</p>
	</PageWrapper>
);

export default NotFound;
