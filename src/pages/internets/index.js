import React from 'react';

// Layout
import PageWrapper from 'components/layout/pageWrapper';
import PageContent from 'styled/layout/pageContent';
import Header from 'components/global/header';

// Styled
import IckyIframe from 'styled/iframe';

// Page Config
export const path = '/internets';
export const exact = true;

// Page Renderer
export default () => (
	<PageWrapper>
		<Header
			backTo="/lunch"
			title="Teh Interwebz"
		/>
		<PageContent>
			<IckyIframe src="https://www.bottlerocketstudios.com/contact" />
		</PageContent>
	</PageWrapper>
);
