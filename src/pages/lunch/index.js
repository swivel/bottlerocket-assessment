import * as lunchViewPage from './view';
import * as lunchListPage from './list';

export {
	lunchViewPage,
	lunchListPage
};
