import { connect } from 'react-redux';

import { getRestaurants } from 'actions/restaurants';

import LunchScreen from './lunch';

// Routing Config
export const path = '/(lunch)?';
export const exact = true;

const mapDispatchToProps = dispatch => ({
	onPageLoad: () => dispatch(getRestaurants())
});

export default connect(null, mapDispatchToProps)(LunchScreen);
