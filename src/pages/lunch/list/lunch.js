import React from 'react';

// Layout
import PageWrapper from 'components/layout/pageWrapper';
import Header from 'components/global/header';
import PageContent from 'styled/layout/pageContent';

// Components
import Restaurants from 'components/restaurants';

const HomePage = ({ onPageLoad }) => (
	<PageWrapper onPageLoad={onPageLoad}>
		<Header title="Lunch Tyme" />
		<PageContent>
			<Restaurants />
		</PageContent>
	</PageWrapper>
);

export default HomePage;
