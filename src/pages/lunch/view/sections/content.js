import React from 'react';

// Layout
import PageContent from 'styled/layout/pageContent';

// Styled
import ListViewHeader from 'styled/listView/header';
import ListViewDetails from 'styled/listView/details';

// Components
import Map from 'components/map';

const LunchViewContentSection = ({ restaurant }) => {
	const {
		contact,
		location: {
			formattedAddress: address,
			lat,
			lng
		}
	} = restaurant;

	// Address
	const locationDisp = (
		<address>
			{address.map(a => <p key={a}>{a}</p>)}
		</address>
	);

	// Phone
	let phoneDisp = null;
	if (contact) {
		const { phone, formattedPhone } = contact;
		if (phone && formattedPhone) {
			phoneDisp = (
				<a href={`tel:+1${phone}`}>{formattedPhone}</a>
			);
		}
	}

	// Twitter
	let twitterDisp = null;
	if (contact) {
		const { twitter } = contact;
		if (twitter) {
			twitterDisp = (
				<a href={`https://twitter.com/${twitter}`}>
					@{twitter}
				</a>
			);
		}
	}

	// Facebook
	let facebookDisp = null;
	if (contact) {
		const {
			facebookUsername,
			facebookName = facebookUsername
		} = contact;
		if (facebookUsername) {
			facebookDisp = (
				<a href={`https://facebook.com/${facebookUsername}`}>
					Facebook: {facebookName}
				</a>
			);
		}
	}

	return (
		<PageContent>
			<Map lat={lat} lng={lng} />
			<ListViewHeader>
				<h1>{restaurant.name}</h1>
				<strong>{restaurant.category}</strong>
			</ListViewHeader>
			<ListViewDetails>
				{locationDisp}
				{phoneDisp}
				{twitterDisp}
				{facebookDisp}
			</ListViewDetails>
		</PageContent>
	);
};

export default LunchViewContentSection;
