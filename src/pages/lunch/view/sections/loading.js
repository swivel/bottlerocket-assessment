import React from 'react';

// Layout
import PageContent from 'styled/layout/pageContent';

const LunchViewLoadingSection = () => (
	<PageContent>
		Loading...
	</PageContent>
);

export default LunchViewLoadingSection;
