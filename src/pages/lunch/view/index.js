import { connect } from 'react-redux';

// Actions
import { getMapsApiKey } from 'actions/mapsApi';
import { getRestaurants } from 'actions/restaurants';

// Component
import ViewScreen from './view';

export const path = '/lunch/:id';
export const exact = true;

const mapStateToProps = ({ restaurantsReducer }) => ({ ...restaurantsReducer });

const mapDispatchToProps = dispatch => ({
	onPageLoad: () => {
		dispatch(getMapsApiKey());
		dispatch(getRestaurants());
	}
});

const mergeProps = (state, dispatch, props) => {
	const { match: { params: { id } } } = props;
	const {
		restaurants,
		loading: loadingFlag
	} = state;

	const loading = restaurants.length === 0 || loadingFlag;

	let restaurant = null;
	let redirect = null;
	if (!loading) {
		const name = id.replace(/[_]/g, ' ');
		restaurant = restaurants.find(r => r.name === name);
		if (!restaurant) {
			redirect = '/lunch';
		}
	}

	return {
		...props,
		...dispatch,
		restaurant,
		redirect,
		loading
	};
};

export default connect(
	mapStateToProps,
	mapDispatchToProps,
	mergeProps
)(ViewScreen);
