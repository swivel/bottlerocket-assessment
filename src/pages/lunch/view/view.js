import React from 'react';

import { Redirect } from 'react-router-dom';

// Layout
import PageWrapper from 'components/layout/pageWrapper';
import Header from 'components/global/header';

// Sections
import LunchViewContentSection from './sections/content';
import LunchViewLoadingSection from './sections/loading';

const ViewScreen = (props) => {
	const {
		loading,
		restaurant,
		redirect,
		onPageLoad
	} = props;

	if (redirect) {
		return (<Redirect to={redirect} />);
	}

	return (
		<PageWrapper onPageLoad={onPageLoad}>
			<Header
				backTo="/lunch"
				title="Lunch Tyme"
			/>
			{loading ? (
				<LunchViewLoadingSection />
			) : (
				<LunchViewContentSection
					restaurant={restaurant}
				/>
			)}
		</PageWrapper>
	);
};

export default ViewScreen;
