// Root Pages
import * as lunchPages from 'pages/lunch';
import * as internetsPage from 'pages/internets';
import * as notFoundPage from 'pages/notFound';

export default {
	...lunchPages,
	internetsPage,
	notFoundPage
};
