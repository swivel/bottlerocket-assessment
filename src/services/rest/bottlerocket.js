// Services
import { getAppConfig } from 'services/config';

export const getRestaurants = () =>
	getAppConfig()
		.then(({ apiHost }) => fetch(`${apiHost}/restaurants.json`))
		.then(response => response.json())
		.then(({ restaurants }) => restaurants);
