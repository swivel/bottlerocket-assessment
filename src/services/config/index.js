const appConfig = (
	fetch('/config.json').then(response => response.json())
);

export const getAppConfig = () => appConfig;
