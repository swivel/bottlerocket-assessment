import React from 'react';
import styled from 'styled-components';

import MapMarkerAlt from './assets/map-marker-alt.svg';

const MapMarker = styled(MapMarkerAlt)`
	svg {
		height: 96px;
	}

	path {
		fill: #34B379;
	}
`;

export default () => (<MapMarker height={96} />);
