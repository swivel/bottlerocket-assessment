import styled from 'styled-components';

const HeaderTitle = styled.h1`
	flex-grow: 1;
	flex-shrink: 1;	

	font-family: 'Avenir Next Demi Bold', 'Avenir Next', 'Helvetica Neue', Arial, sans-serif;
	font-weight: 600;
	font-size: 1.5rem;
	color: #fff;
	text-align: center;
`;

export default HeaderTitle;
