import React from 'react';
import styled from 'styled-components';
import { NavLink } from 'react-router-dom';

const commonStyles = `
	display: block;
	height: 48px;
	width: 48px;
`;

const NullIcon = styled.span`${commonStyles}`;

const IconLink = styled(NavLink)`
	${commonStyles}

	&::after {
		content: '';
		display: block;
		background: url('/assets/imgs/${({ icon }) => icon}@2x.png');
		background-size: contain;
		background-repeat: no-repeat;
		background-position: center center;
		height: 48px;
		width: 100%;
	}
`;

export default (props) => {
	const { to } = props;
	if (!to) return <NullIcon />;
	return (
		<IconLink {...props} />
	);
};
