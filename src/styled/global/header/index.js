import styled from 'styled-components';

const HeaderStyled = styled.header`
	flex-basis: 8vh;
	flex-shrink: 0;
	flex-grow: 0;

	display: flex;
	flex-direction: row;
	justify-content: space-between;
	align-items: center;
	padding: 0px 5vw;

	background: #43E895;
`;

export default HeaderStyled;
