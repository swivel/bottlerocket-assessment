import styled from 'styled-components';

const ActionBar = styled.nav`
	flex-basis: 10vh;

	display: flex;
	align-items: stretch;
	justify-content: space-evenly;

	background: #2a2a2a;
`;

export default ActionBar;
