import React from 'react';
import styled from 'styled-components';

// Compopnents
import { NavLink } from 'react-router-dom';

const LinkStyled = styled(NavLink)`
	text-decoration: none;
	color: #979797;
	font-family: 'Avenir Next Regular', 'Avenir Next', 'Helvetica Neue', 'Arial', sans-serif;
	font-size: 1.0rem;
	font-weight: 500;
	text-transform: lowercase;

	display: flex;
	flex-direction: column;
	align-items: center;
	justify-content: space-evenly;

	&.active {
		color: #FFF;

		&::before {
			opacity: 1;
		}
	}

	&::before {
		${({ icon }) => icon && `
			content: '';
			background-image: url('/assets/imgs/${icon}@2x.png');
		`}
		height: 64px;
    	width: 100%;
		opacity: 0.5;
    	background-size: contain;
    	background-repeat: no-repeat;
		background-position: center center;
	}
`;

const Link = props => (
	<LinkStyled
		activeClassName="active"
		{...props}
	/>
);

export default Link;
