import styled from 'styled-components';

const PageContent = styled.div`
	/* parent container flex */
	flex-basis: 82vh;
	flex-grow: 1;
	flex-shrink: 0;

	overflow-y: scroll;
`;

export default PageContent;
