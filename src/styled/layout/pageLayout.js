import styled from 'styled-components';

const PageLayout = styled.main`
	flex-basis: 90vh;
	flex-shrink: 0;
	flex-grow: 0;

	display: flex;
	flex-direction: column;
	align-content: stretch;
`;

export default PageLayout;
