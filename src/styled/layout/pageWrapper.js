import styled from 'styled-components';

const PageWrapperStyled = styled.div`
	display: flex;
	flex-direction: column;

	min-height: 100vh;
	min-width: 100%;
`;

export default PageWrapperStyled;
