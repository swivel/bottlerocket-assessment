import styled from 'styled-components';

const IckyIframe = styled.iframe`
	flex-basis: 100%;
	flex-shrink: 0;
	flex-grow: 1;
	align-self: stretch;
	height: 82vh;
	width: 100%;
	overflow-y: scroll;
`;

export default IckyIframe;
