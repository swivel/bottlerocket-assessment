import styled from 'styled-components';

const ListViewDetails = styled.section`
	padding: 3vh 2vh;

	font-family: 'Avenir Next Regular', 'Avenir Next', 'Helvetica Neue', 'Arial', sans-serif;
	font-size: 1.0rem;
	font-weight: 400;
	color: #2a2a2a;

	& > * {
		margin: 2vh 0;
	}

	& > *:first-child {
		margin-top: 0;
	}

	a {
		display: block;
		text-decoration: none;
		color: inherit;
	}
`;

export default ListViewDetails;
