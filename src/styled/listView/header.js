import styled from 'styled-components';

const ListViewHeader = styled.header`
	color: #fff;
	background: #34B379;
	padding: 2vh;


	h1 {
		font-family: 'Avenir Next Demi Bold','Avenir Next', 'Helvetica Neue', 'Arial', sans-serif;
		font-size: 1.3rem;
		font-weight: 700;
		margin-bottom: 1vh;
	}

	strong {
		font-family: 'Avenir Next Regular','Avenir Next', 'Helvetica Neue', 'Arial', sans-serif;
		font-weight: 500;
		font-size: 1.1rem;
	}
`;

export default ListViewHeader;
