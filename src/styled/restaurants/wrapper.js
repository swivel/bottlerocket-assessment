import styled from 'styled-components';

const RestaurantsWrapperStyled = styled.section`
	@media screen and (orientation:landscape) {
		display: flex;
		flex-wrap: wrap;

		article {
			width: 50%;
		}
	}
`;

export default RestaurantsWrapperStyled;
