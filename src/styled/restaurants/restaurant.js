import React from 'react';
import styled from 'styled-components';
import { NavLink } from 'react-router-dom';

const RestaurantStyled = styled.article`
	a {
		display: flex;
		flex-direction: column;
		justify-content: flex-end;

		position: relative;
		overflow: hidden;

		height: 30vh;
		width: 100%;

		padding: 1.5vh;

		color: #FFF;
		text-decoration: none;

		&::after {
			content: '';
			display: block;
			position: absolute;
			top: 0;
			left: 0;
			width: 100%;
			height: 100%;
			z-index: -1;
			background-image: linear-gradient(to bottom, rgba(0,0,0,0), rgba(0,0,0,0.5));
		}
	}

	img {
		position: absolute;
		top: 0;
		left: 0;

		min-height: 100%;
		min-width: 100%;
		
		z-index: -1;
	}

	h1 {
		font-family: 'Avenir Next Demi Bold','Avenir Next', 'Helvetica Neue', 'Arial', sans-serif;
		font-size: 1.4rem;
		font-weight: 700;
		margin-bottom: 1vh;
	}

	strong {
		font-family: 'Avenir Next Regular','Avenir Next', 'Helvetica Neue', 'Arial', sans-serif;
		font-weight: 500;
		font-size: 1.2rem;
	}
`;

const Restaurant = ({ to, children }) => (
	<RestaurantStyled>
		<NavLink to={to}>
			{children}
		</NavLink>
	</RestaurantStyled>
);

export default Restaurant;
