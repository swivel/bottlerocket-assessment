# Bottle Rocket Assessment

## Getting Started

Clone this repository and install dependencies using `yarn`:
```shellsession
user@host ~/Projects$ git clone https://gitlab.com/swivel/bottlerocket-assessment.git assessment
user@host ~/Projects$ cd assessment
user@host assessment$ yarn
```

Create a `config.json` in the project root, and add the following:
```json
{
	"apiHost": "http://localhost:8080/assets",
	"mapsApi": "PASTE_GOOGLE_MAPS_API_KEY_HERE"
}
```

Setting `apiHost` to the URL provided by Bottle Rocket's Assessment documentation failed due to CORS, so use as your own risk.


Start the server locally by running:
```shellsession
user@host assessment$ yarn start
```
